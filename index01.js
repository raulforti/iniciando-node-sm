let express = require('express');
const { request } = require('http');
let app = express();
const port = 8000;

// recurso principal
app.get('/',(request,response)=>{
    response.send("(message: método get)");
});

// recurso funcionário
app.get('/funcionario',(request,response)=>{
    let obj = request.query;
    let nome = obj.nome;
    let sobrenome = obj.sobrenome;
    response.send(`(message: método get funcionário ${nome} ${sobrenome})`);
});

// habilita o serviço express na porta 8000
app.listen(port,function(){
    console.log(`Projeto rodando na porta: ${port}`);
});

